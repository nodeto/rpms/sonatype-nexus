FROM docker.io/rockylinux/rockylinux:8 as buildenv

RUN dnf install -y \
    yum-utils \
    && dnf clean all

RUN yum-config-manager \
    --setopt=baseos.baseurl=https://nexus.nodeto.site/repository/rocky/8.6/BaseOS/x86_64/os \
    --setopt=appstream.baseurl=https://nexus.nodeto.site/repository/rocky/8.6/AppStream/x86_64/os \
    --setopt=extras.baseurl=https://nexus.nodeto.site/repository/rocky/8.6/extras/x86_64/os \
    --setopt=powertools.baseurl=https://nexus.nodeto.site/repository/rocky/8.6/PowerTools/x86_64/os \
    --setopt=baseos.mirrorlist= \
    --setopt=appstream.mirrorlist= \
    --setopt=extras.mirrorlist= \
    --setopt=powertools.mirrorlist= \
    --save

RUN yum-config-manager --enable powertools

RUN dnf install -y \
    rpm-build \
    rpmdevtools \
    dnf-utils \
    yum-utils \
    && dnf clean all

RUN rpmdev-setuptree

COPY SPECS/dummy.spec /root/rpmbuild/SPECS/
COPY SPECS/requires.spec /root/rpmbuild/SPECS/
RUN yum-builddep -y /root/rpmbuild/SPECS/dummy.spec

ARG PACKAGE_VERSION
ENV PACKAGE_VERSION=$PACKAGE_VERSION
RUN echo $PACKAGE_VERSION
COPY SPECS/ /root/rpmbuild/SPECS/
COPY SOURCES/ /root/rpmbuild/SOURCES/
RUN rpmspec -P /root/rpmbuild/SPECS/sonatype-nexus.spec \
    && rpmbuild -ba --noclean /root/rpmbuild/SPECS/sonatype-nexus.spec

ARG YUM_USER
ARG YUM_PASSWORD
RUN curl -f -u ${YUM_USER}:${YUM_PASSWORD} --upload-file /root/rpmbuild/RPMS/x86_64/nodeto.com-sonatype-nexus-3.40.1-8.el8.x86_64.rpm \
        https://nexus.nodeto.site/repository/yum/
