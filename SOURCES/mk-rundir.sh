#!/bin/sh

/usr/bin/mkdir -p /run/user/$(/usr/bin/id sonatype-nexus -u)
/usr/bin/chown $(/usr/bin/id sonatype-nexus -u):$(/usr/bin/id sonatype-nexus -g) \
    /run/user/$(/usr/bin/id sonatype-nexus -u)
/usr/bin/chmod 0700 /run/user/$(/usr/bin/id sonatype-nexus -u)
