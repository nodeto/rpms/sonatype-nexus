Requires:           nodeto.com-podman-compose
Requires:           systemd

BuildRequires:      systemd-rpm-macros

Requires(post):     shadow-utils
Requires(post):     python38
Requires(post):     grep
Requires(post):     policycoreutils
Requires(post):     policycoreutils-python-utils
Requires(post):     container-selinux

Requires(postun):   python38
Requires(postun):   policycoreutils-python-utils
Requires(postun):   podman
Requires(postun):   util-linux
