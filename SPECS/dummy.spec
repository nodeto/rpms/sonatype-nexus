
Name:           nodeto.com-dummy
Version:        1.0
Release:        1%{?dist}
Summary:        Dummy App
BuildArch:      x86_64

License:        GPL
Source0:        dummy.fake

%include /root/rpmbuild/SPECS/requires.spec

%description
This is a spec file so that dependency installation can be in the cache

%global debug_package %{nil}
%prep
%setup -q

%install
true

%files
true
