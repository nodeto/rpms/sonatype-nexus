%define appname sonatype-nexus

Name:           nodeto.com-%{appname}
Version:        %(echo $PACKAGE_VERSION)
Release:        8%{?dist}
Summary:        Sonatype Nexus
BuildArch:      x86_64

License:        GPL
Source0:        sonatype-nexus.yaml
Source1:        mk-rundir.sh
Source2:        sonatype-nexus.service
Source3:        nodeto.com-sonatype-nexus.sysuser

%include /root/rpmbuild/SPECS/requires.spec

%description
Sonatype Nexus is an artifact repository and repository proxy.

%global debug_package %{nil}
%define sysuser_home /var/opt/lib/nodeto.com/sonatype-nexus
%define containers_dir %{sysuser_home}/.local/share/containers
%install
rm -rf %{buildroot}
install -p -D -m 0644 %{SOURCE0} %{buildroot}/opt/nodeto.com/%{appname}/compose/sonatype-nexus.yaml
install -p -D -m 0755 %{SOURCE1} %{buildroot}/opt/nodeto.com/%{appname}/bin/mk-rundir.sh
install -p -D -m 0644 %{SOURCE2} %{buildroot}%{_unitdir}/sonatype-nexus.service
install -p -D -m 0644 %{SOURCE3} %{buildroot}%{_sysusersdir}/sonatype-nexus.conf
install -p -D -m 0700 -d %{buildroot}%{sysuser_home}
install -p -D -m 0700 -d %{buildroot}%{sysuser_home}/.local
install -p -D -m 0700 -d %{buildroot}%{sysuser_home}/.local/share
install -p -D -m 0700 -d %{buildroot}%{containers_dir}

%files
%dir /opt/nodeto.com/
%{_sysusersdir}/sonatype-nexus.conf
%{_unitdir}/sonatype-nexus.service
/opt/nodeto.com/%{appname}/*

%defattr(0644, sonatype-nexus, sonatype-nexus, 0700)
%dir %{sysuser_home}
%dir %{sysuser_home}/.local
%dir %{sysuser_home}/.local/share
%dir %{containers_dir}

%pre
%sysusers_create_package %{appname} %{SOURCE3}

%post
if ! grep sonatype-nexus /etc/subuid >/dev/null; then
    SYSUID="$(id sonatype-nexus -u)"
    SUBUIDS=$(/usr/bin/python3.8 -c "print(f'{($SYSUID + 30000) * 2**16}-{($SYSUID + 30001) * 2**16 - 1}')")
    /usr/sbin/usermod --add-subuids "$SUBUIDS" sonatype-nexus
fi
if ! grep sonatype-nexus /etc/subgid >/dev/null; then
    SYSGID="$(id sonatype-nexus -g)"
    SUBGIDS=$(/usr/bin/python3.8 -c "print(f'{($SYSGID + 30000) * 2**16}-{($SYSGID + 30001) * 2**16 - 1}')")
    /usr/sbin/usermod --add-subgids "$SUBGIDS" sonatype-nexus
fi

/usr/sbin/semanage fcontext -a -e /var/lib/containers %{containers_dir}  >/dev/null 2>&1
/usr/sbin/restorecon -R %{containers_dir}

%systemd_post sonatype-nexus.service

%preun
%systemd_preun sonatype-nexus.service

%postun
%systemd_postun_with_restart sonatype-nexus.service
/usr/sbin/semanage permissive -a rpm_script_t >/dev/null
su -s /bin/bash sonatype-nexus -c "/usr/bin/podman image rm docker.io/sonatype/nexus3:%{version}" >/dev/null
/usr/sbin/semanage permissive -d rpm_script_t >/dev/null
